$(document).ready(function(){

	//FilmSlider();

	function FilmSlider(){

		$('#filmList .film-list-item').on('click', function(){

			base = $(this);

			//Get Film data
			poster 	= base.attr('data-film-poster');
			title 	= base.attr('data-film-title');
			content = base.attr('data-film-content');
			genre 	= base.attr('data-film-genre');
			rating1 = base.attr('data-film-rating-1');
			rating2 = base.attr('data-film-rating-2');
			rating3 = base.attr('data-film-rating-3');
			rating4 = base.attr('data-film-rating-4');
			trailer = base.attr('data-film-trailer');
			url 	= base.attr('data-film-url');


			$('#film .film-banner img').attr('url', poster);
			$('#film .film-body h2').text(title);
			$('#film .film-genre p').text(genre);
			$('#film .film-body-text p').text(content);

			$('#film .film-body-rating .rating-cinema span').text(rating1);
			$('#film .film-body-rating .rating-tomato span').text(rating2);
			$('#film .film-body-rating .rating-imdb span').text(rating3);
			$('#film .film-body-rating .rating-rk span').text(rating4);
			$('#film .film-body-text p').text(content);
			$('#film .film-body-text p').text(content);

			$('#film .film-body-footer a.btn-1').attr('href', trailer);
			$('#film .film-body-footer a.btn-2').attr('href', url);




		});
	}

});