$(document).ready(function(){

	App();
	function App(){

		GetMoreGenre();

		SelectGenre();

		GetRandomGenreFilm();

		Search();

		GetMoreCountry();

		SelectCountry();


		filmSlider();

		ThemeColor();

		function ThemeColor(){
			$('.layout').on('click', '.switch-wrapper input[type=checkbox], .switch.ui-btn input[type=checkbox]', function(){
				if($(this).is(":checked")) {
					$('.switch-wrapper input[type=checkbox]').not(this).attr('checked', 'checked'); 
					
				}
				else{
					$('.switch-wrapper input[type=checkbox]').not(this).removeAttr("checked");; 
					
				}
				$('body').toggleClass('theme-dark');
			})
			
		}


		// GenreShowHide();


		function isMobile() {
		    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		          return true; 
		    }
		    return false;
		}




		

	    //GetSearch
	    $('#getSearch').on('click', function(){
	        $('#headerSearch').toggleClass('search-open');

	        $('#getNav').toggleClass('hide');
	        $('.js-header-mobile-logo').toggleClass('search-open');
	    });

	    $('#searchClose').on('click', function(){
	        $('#headerSearch').toggleClass('search-open');
	        $('#getNav').toggleClass('hide');
	        $('.header-search-wrapper').removeClass('search-open');
	        $('.js-header-mobile-logo').toggleClass('search-open');
	        $('.js-header-search-wrapper .js-search-inpt').val('');
	    });  

		function GetMoreGenre(){
			$('#getMoreGenre').on('click', function(){

				$('.genre-more-inner').toggleClass('open');

			})
		}

		function SelectGenre(){
			$('#getMoreGenreList a').on('click', function(){

				base = $(this);

				text = base.text();

				$('#getMoreGenre').text(text);

				$('.js-genre-more-inner').toggleClass('open');

			})
		}



		function GetRandomGenreFilm(){
			$('#getRandomFilmGenre').on('click', function(){

				$('.js-film-random-genre').toggleClass('open');

			})


			$('.js-film-random-genre li').on('click', function(){

				base = $(this);

				val = base.text();

				base.parents('.js-film-random-genre').find('#getRandomFilmGenre span').text(val);
				base.parents('.js-film-random-genre').toggleClass('open');

			})
		}

		function GetMoreCountry(){
			$('.js-select-country-selected').on('click', function(){

				$('.js-select-country').toggleClass('open');

			})
		}

		function SelectCountry(){
			$('.js-select-country-list li').on('click', function(){

				base = $(this);

				text = base.text();

				$('.js-select-country-selected').text(text);

				$('.js-select-country').toggleClass('open');

			})
		}


		//Search
		function Search(){

			$('.js-header-search input').on('keyup', function(){

		        base = $(this);
		        word_lenght = base.val().length;

		        if( word_lenght > 2 ){
		        	$('.js-header-search-result').slideDown(100);
		        	$('.header-search-wrapper').addClass('search-open');
		        }
		        else{
		        	$('.js-header-search-result').slideUp(100);
		        	$('.header-search-wrapper').removeClass('search-open');
		        }

		    });

		}


		function filmSlider(){
			itemCount = $('#filmList .film-list-item').length;

			//Set default data after load page:
			$('.js-film').attr('data-film-count', 0);
			$('#filmList .film-list-item').removeClass('active');
			$('#filmList .film-list-item').eq(0).addClass('active');


			// if( $('#film').attr('data-film-count') == 0 ){
			// 	$('.film-btn-arrow-prev').addClass('btn-hide');
			// }


			//Next FILM
			$('body').on('click', '.film-btn-arrow-next', function(){

				currentFilm = $('.js-film').attr('data-film-count')*1;



				currentFilm += 1;

				if( currentFilm < itemCount ){

					$('.js-film').attr('data-film-count', currentFilm);
					filmData = GetFilmData(currentFilm);
					SetFilmData(filmData);
					$('#filmList .film-list-item').removeClass('active');
					$('#filmList .film-list-item').eq(currentFilm).addClass('active');

				}

				
			});

			//Prev FILM
			$('.film-btn-arrow-prev').on('click', function(){

				currentFilm = $('.js-film').attr('data-film-count')*1;

				currentFilm -= 1;

				if( currentFilm >= 0 && currentFilm < itemCount ){

					filmData = GetFilmData(currentFilm);
					SetFilmData(filmData);
					$('.js-film').attr('data-film-count', currentFilm);
					$('#filmList .film-list-item').removeClass('active');
					$('#filmList .film-list-item').eq(currentFilm).addClass('active');

				}
				
			})

			//Current Film
			$('#filmList .film-list-item').on('click', function(){

				$('#filmList .film-list-item').removeClass('active');
				$(this).addClass('active');

				currentFilm = $(this).index();
				filmData = GetFilmData(currentFilm);
				SetFilmData(filmData);

				$('.js-film').attr('data-film-count', currentFilm);

				if( isMobile() ){
					$('#mobileFilmShow').modal();
				}

				
				
			});
			
		}

		function GetFilmData(item){

			

			filmData = new Array();

			filmData['poster'] = $('#filmList .film-list-item').eq(item).attr('data-film-poster');
			filmData['title'] = $('#filmList .film-list-item').eq(item).attr('data-film-title');
			filmData['content'] = $('#filmList .film-list-item').eq(item).attr('data-film-content');
			filmData['genbre'] = $('#filmList .film-list-item').eq(item).attr('data-film-genre');
			filmData['rating1'] = $('#filmList .film-list-item').eq(item).attr('data-film-rating-1');
			filmData['rating1'] = $('#filmList .film-list-item').eq(item).attr('data-film-rating-2');
			filmData['rating3'] = $('#filmList .film-list-item').eq(item).attr('data-film-rating-3');
			filmData['rating4'] = $('#filmList .film-list-item').eq(item).attr('data-film-rating-4');
			filmData['trailer'] = $('#filmList .film-list-item').eq(item).attr('data-film-trailer');
			filmData['url'] = $('#filmList .film-list-item').eq(item).attr('data-film-url');

			// console.log(filmData.poster);
			return filmData;

		}


		function SetFilmData(data){

			$('.js-film-shadow').addClass('show');

				 setTimeout(function(){
		            $('.js-film-shadow').removeClass('show');

		            $('.js-film .film-banner .js-film-banner').attr('src', data.poster);
					$('.js-film .film-body h2').text(data.title);
					$('.js-film .film-genre p').text(data.genre);
					$('.js-film .film-body-text p').text(data.content);

					$('.js-film .film-body-rating .rating-cinema span').text(data.rating1);
					$('.js-film .film-body-rating .rating-tomato span').text(data.rating2);
					$('.js-film .film-body-rating .rating-imdb span').text(data.rating3);
					$('.js-film .film-body-rating .rating-rk .js-rating-rk').text(data.rating4);
					$('.js-film .film-body-text p').text(data.content);
					$('.js-film .film-body-footer a.btn-1').attr('href', data.trailer);
					$('.js-film .film-body-footer a.btn-2').attr('href', data.url);
		    },500);


			

		}


		function GenreShowHide(){

			itemCount = $('.js-genre-list li').length;
			boxWidth = Math.floor($('.js-genre-list').width());
			boxWidth2Column = boxWidth * 2;
			elementsWidth = 0;

			for (i = 0; i < itemCount; ) {

				elementsWidth += $('.js-genre-list li').eq(i).outerWidth();

				if( elementsWidth < ( boxWidth - 20 ) ){}

				if( elementsWidth >= boxWidth2Column || i > 6 ) { break; }

			  i++;

			  console.log( '--Iteration: ' + i + ' elementsWidth: ' + elementsWidth + " boxWidth2Column: " + boxWidth2Column );
			}


		}



		//Mobile nav:
     	function handler1() {
	        $('#sidebar').addClass('open').animate({
	            right: "0"
	        },400);            
	        $(this).addClass('open');
	        $(this).one("click", handler2);
	        $('body, .shadow').addClass('open');
	    }
	    function handler2() {
	        $('#sidebar').removeClass('open').animate({
	            right: "-300"
	        },400);
	        $('#getNav').removeClass('open');
	        $('#getNav').one("click", handler1);
	        $('body, .shadow').removeClass('open');
	    }               
	    $(document).on( 'keydown', function ( e ) {
	        if ( e.keyCode === 27 ) { // ESC
	            handler2();
	        }
	    });
	    $("#getNav").one("click", handler1);
	    $('#shadow').on('click', function(){
	        handler2();
	    });
		

		$(".header").on("swiperight", function(){
			handler1();
			$('#getNav').addClass('open');
		});  



		document.addEventListener('touchstart', handleTouchStart, false);        
		document.addEventListener('touchmove', handleTouchMove, false);

		var xDown = null;                                                        
		var yDown = null;

		function getTouches(evt) {
		  return evt.touches ||             // browser API
		         evt.originalEvent.touches; // jQuery
		}                                                     

		function handleTouchStart(evt) {
		    const firstTouch = getTouches(evt)[0];                                      
		    xDown = firstTouch.clientX;                                      
		    yDown = firstTouch.clientY;                                      
		};                                                

		function handleTouchMove(evt) {
		    if ( ! xDown || ! yDown ) {
		        return;
		    }

		    var xUp = evt.touches[0].clientX;                                    
		    var yUp = evt.touches[0].clientY;

		    var xDiff = xDown - xUp;
		    var yDiff = yDown - yUp;

		    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
		        if ( xDiff > 0 ) {


		            /* left swipe */ 
		        } else {
		        	handler1();
		        	$('#getNav').addClass('open');
		            /* right swipe */
		        }                       
		    } else {
		        if ( yDiff > 0 ) {
		            /* up swipe */ 
		        } else { 
		            /* down swipe */
		        }                                                                 
		    }
		    /* reset values */
		    xDown = null;
		    yDown = null;                                             
		};




	};


   



});


$(document).on("pagecreate","#pageone",function(){

});






